package com.example.victwo;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.util.Log;

public class Audio {
	private MediaPlayer mPlayer;
	private MediaRecorder mRecorder;
	private boolean recordingMade = false;
	
	public String path;

	public boolean getRecordingMade() {
		return recordingMade;
	}
	
	public void setRecordingMade(boolean flag) {
		this.recordingMade = flag;
	}
	
	public void setPath(String path) {
		this.path = path;
	}

	public void stopPlayRecorded() {
		if (mPlayer != null) {
			mPlayer.stop();
		}
	}

	public void playRecorded() throws IllegalArgumentException,
			SecurityException, IllegalStateException, IOException {
		
		ditchMediaPlayer();
		
		FileClass f = new FileClass();
		
		if(!f.isExistingFileOrDirectory(path)) {
			return;
		}
		
		mPlayer = new MediaPlayer();
		mPlayer.setDataSource(path);
		mPlayer.prepare();
		mPlayer.start();
		
	}

	public MediaPlayer getMPlayer() {
		return mPlayer;
	}
	
	private void ditchMediaPlayer() {
		if (mPlayer != null) {
			try {
				mPlayer.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void stopRecording() {
		if (mRecorder != null) {
			recordingMade = true;
			mRecorder.stop();
			ditchMediaRecorder();
		}

	}

	public void beginRecording() throws IllegalStateException, IOException {
		File f = new File(path.substring(0, path.lastIndexOf("/")));

		if (!recordingExists(f)) {
			f.mkdirs();
		}

		ditchMediaRecorder();
		File outFile = new File(path);

		Log.d("R_PATH", path);

		if (outFile.exists()) {
			outFile.delete();
		}

		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mRecorder.setOutputFile(path);

		mRecorder.prepare();
		mRecorder.start();
	}

	public boolean recordingExists(File f) {
		if (f.exists())
			return true;
		else
			return false;
	}
	
	private void ditchMediaRecorder() {
		if (mRecorder != null) {
			mRecorder.reset();
			mRecorder.release();
			mRecorder = null;
		}
	}
}