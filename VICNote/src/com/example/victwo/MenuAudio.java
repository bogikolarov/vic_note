package com.example.victwo;

import android.R;
import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;

public class MenuAudio extends ActionProvider implements OnMenuItemClickListener {

	private Context noteContext;
	
	public MenuAudio(Context context) {
		super(context);
		noteContext = context;
	}

	@Override
	public View onCreateActionView() {
		 ImageView imageView = new ImageView(noteContext);
	     imageView.setImageResource(R.drawable.ic_menu_call);
		return null;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.ActionProvider#onCreateActionView(android.view.MenuItem)
	 */
/*	@Override
	public View onCreateActionView(MenuItem forItem) {
		
		return super.onCreateActionView(forItem);
	}
*/
	/* (non-Javadoc)
	 * @see android.support.v4.view.ActionProvider#onPrepareSubMenu(android.view.SubMenu)
	 */
	@Override
	public void onPrepareSubMenu(SubMenu subMenu) {
		//super.onPrepareSubMenu(subMenu);
		subMenu.clear();
		
		subMenu.add("Record").setOnMenuItemClickListener(this);
		subMenu.add("Stop Record").setOnMenuItemClickListener(this);
		subMenu.add("Play").setOnMenuItemClickListener(this);
		subMenu.add("Stop Playing").setOnMenuItemClickListener(this);	
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		// TODO Auto-generated method stub
		return false;
	}

}
