package com.example.victwo;

import java.io.IOException;

import com.elsys.vicnote.R;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class PopupAudio extends Activity implements OnClickListener {
	
	LinearLayout layoutOfPopup;
	PopupWindow popupMessage;
	Button popRecord, popPlay, popDismiss;
	TextView popupText;
	Context con;
	
	Audio audio;
	boolean startStopRecord = true, startStopPlaying = true;
	public boolean popupAudio = true;
	
	public PopupAudio(Audio audio) {
		this.audio = audio;
	}

	public void showPopUp(View anchor) {
		popupMessage.showAsDropDown(anchor, 0, 0);
	}
	
	@SuppressLint("InlinedApi")
	public void popupInit() {
		popRecord.setBackgroundResource(R.drawable.ic_action_mic);
		popRecord.setOnClickListener(this);
		popPlay.setOnClickListener(this);
		popDismiss.setOnClickListener(this);
		popupMessage = new PopupWindow(layoutOfPopup, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		popupMessage.setContentView(layoutOfPopup);
		//popupMessage.setBackgroundDrawable(R.drawable.popup_layout);
	}

	public void popupDismiss() {
		popupMessage.dismiss();
	}
	
	public void init(Context context) {
		con = context;
		popRecord = new Button(context);
		popRecord.setId(7368786);
		popRecord.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
	            ViewGroup.LayoutParams.MATCH_PARENT));
		
		popPlay = new Button(context);
		popPlay.setBackgroundResource(R.drawable.ic_action_play);
		popPlay.setId(7368784);
		popPlay.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
	            ViewGroup.LayoutParams.MATCH_PARENT));

		popDismiss = new Button(context);
		popDismiss.setBackgroundResource(R.drawable.ic_action_hide);
		popDismiss.setId(7368772);
		popDismiss.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
	            ViewGroup.LayoutParams.MATCH_PARENT));
		
		layoutOfPopup = new LinearLayout(context);
		layoutOfPopup.setBackgroundResource(R.drawable.popup_shape);
		layoutOfPopup.addView(popRecord);
		layoutOfPopup.addView(popPlay);
		layoutOfPopup.addView(popDismiss);
	}

	private void onStopPlaying() {
		popPlay.setBackgroundResource(R.drawable.ic_action_play);
		startStopPlaying = true;
	}
	
	@Override
	public void onClick(View v) {

		switch(v.getId()) {
			case 7368786:
				popRecord.setBackgroundResource(R.drawable.ic_action_stop);
				if(startStopRecord) {
					try {
						audio.beginRecording();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					startStopRecord = false;
				}else {
					Log.d("STOP", "REC");
					audio.stopRecording();
					audio.setRecordingMade(true);
					startStopRecord = true;
					popRecord.setBackgroundResource(R.drawable.ic_action_mic);
				}
				
				break;
				
			case 7368784:
				FileClass fc = new FileClass();
				MediaPlayer mPlayer;
				
				if(fc.isExistingFileOrDirectory(audio.path)) {
					if(startStopPlaying) {
						try {
							popRecord.setEnabled(false);
							audio.playRecorded();
							popPlay.setBackgroundResource(R.drawable.ic_action_stop);
							startStopPlaying = false;
							
							mPlayer = audio.getMPlayer();
							mPlayer.setOnCompletionListener(new OnCompletionListener() {
								
								@Override
								public void onCompletion(MediaPlayer mp) {
									// TODO Auto-generated method stub
									onStopPlaying();
									popRecord.setEnabled(true);
								}
							});
							
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalStateException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else {
						Log.d("PLAY", "STOP");
						audio.stopPlayRecorded();
						onStopPlaying();
						popRecord.setEnabled(true);
					}
				}else {
					Toast toast = Toast.makeText(con, "No recording found.", Toast.LENGTH_SHORT);
					toast.show();
					break;
				}
				break;
				
			case 7368772:
				popupAudio = true;
				popupDismiss();
				break;
		}
	}
	
}
