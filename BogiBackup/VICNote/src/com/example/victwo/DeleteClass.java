package com.example.victwo;

import java.io.IOException;

import android.os.Environment;
import android.util.Log;
 	

public class DeleteClass {
	String textTitle;
	String notePath;
	
	public DeleteClass(String text, String title){
		super();
		this.textTitle = title;
		
		DeleteNote(textTitle);
	}
	
	private void DeleteNote(String title) {
		notePath = Environment.getExternalStorageDirectory().toString() + "/VICNote";
		Log.d("DELETE_FILE", title);

		try {
			Log.d("DELETE", "cd " + notePath + " && " + "rm -r " + title.replace(".txt",""));
			Runtime.getRuntime().exec(new String[] {
						    "sh", "-c", "cd " + notePath + " && rm -r \"$1\"", "--", title.replace(".txt","")
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
}