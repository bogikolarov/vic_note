package com.example.victwo;

import com.elsys.vicnote.R;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.graphics.Bitmap;

public class CameraClass extends NoteActivity {

	Intent intent;
	final static int photo = 0;
	Bitmap bmp;
	
	protected CameraClass(){
		super();
		setContentView(R.layout.activity_note);
		Log.d("Creating", "Camera Class");
		intent = new Intent("android.provider.MediaStore.ACTION_IMAGE_CAPTURE");
		Log.d("Initialize", "photo");
		startActivityForResult(intent, photo);
		//startActivity(intent);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			Bundle extras = data.getExtras();
			bmp = (Bitmap) extras.get("data");
			//iv.setImageBitmap(bmp);
		}
	}

}
