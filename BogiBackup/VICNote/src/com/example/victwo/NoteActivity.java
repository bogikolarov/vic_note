package com.example.victwo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.elsys.vicnote.R;

public class NoteActivity extends ActionBarActivity implements OnClickListener {
	boolean play = false;
	boolean record = true;
	
	EditText textData, textTitle;

	String text, title = "";
	String path = Environment.getExternalStorageDirectory().toString() + "/VICNote";
	
	Audio audio = new Audio();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);
		textTitle = (EditText) findViewById(R.id.editTitle);
		
		if(isExistingNote()) {
			Bundle b = getIntent().getExtras();
			Note note = new Note(b);
			
			title = note.getTitle();
			text = note.getTextData();
			textTitle.setText(title.replace(".txt",""));

            if(!text.equals("")) {
	            textData = (EditText) findViewById(R.id.editNote);
	            //Log.d("COLLECTED", "text = " + collected);
	            textData.setText(text);
            }
            
			Log.d("EDIT", "File: " + title);
		}
		
		//---------------------------------------------------------------------
		
		textTitle.setHint("Enter title..");
		
		textData = (EditText) findViewById(R.id.editNote);
		textData.setHint("Enter your note..");
		
	}
	
	@SuppressLint("SimpleDateFormat")
	private String getDateAndTime() {
		String dateTime = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date());
		Log.d("GET DATE", dateTime);
		return dateTime;
	}
	
	private boolean isExistingNote() {
		if(getIntent() != null && getIntent().getExtras() != null)
			return true;
		else
			return false;
	}
	
	private void toastShow(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	private void endActivity() {
			Intent newNote = new Intent(NoteActivity.this, MainActivity.class);
			startActivity(newNote);
			finish();
		}
	
	private void saveNote() {
		text = textData.getText().toString();
		
		setTitle();
		
		new FileClass(text, title);
		toastShow("Saved.");	
	}
	
	private void setTitle() {
		title = textTitle.getText().toString();
		
		if(title.equals("")) {
			title = getDateAndTime();
		}

	}
	
	
	@Override	
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.note, menu);
		return true;
	}


	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			endActivity();
			Log.d("BACK", "Back button pressed");
		}
		return super.onKeyUp(keyCode, event);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		switch(id) {				
			case R.id.action_camera:
				new CameraClass();
				return true;
				
			case R.id.action_audio:
				setTitle();
				String audioPath = path + "/" + title.replace(".txt", "") + "/" + title.replace(".txt", "") + ".3gpp";
				
				Log.d("AUDIO_PATH", audioPath);
				audio.setPath(audioPath);
				
				PopupAudio popup = new PopupAudio(audio);
				popup.init(getApplicationContext());
				popup.popupInit();
				popup.showPopUp(this.findViewById(R.id.action_audio));
				
				return true;
				
			case R.id.action_delete:
				new DeleteClass(text, title);
				toastShow("Note deleted.");
				endActivity();
				return true;

			case R.id.action_save: 
				saveNote();
				endActivity();
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
}