package com.example.victwo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Environment;
import android.util.Log;

public class SaveClass {

	String textData, textTitle;
	int duration;
	
	public SaveClass(String text, String title) {
		super();
		this.textData = text;
		this.textTitle = title;
		
		SaveNote(textData, textTitle);
	}
	
	private void SaveNote(String text, String title) {
		try {
			File root = new File(Environment.getExternalStorageDirectory(), "VICNote" );

			if (!root.exists()) {
	            root.mkdirs();
	        }
			
			File forSave = new File(root, title + ".txt");
	        FileWriter writer = new FileWriter(forSave);
	        Log.v("SAVE", "saving" + title);
	        writer.append(textData);
	        writer.flush();
	        writer.close();
			
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}
	
}