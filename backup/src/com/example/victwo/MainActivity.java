package com.example.victwo;

import java.io.File;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity  {

	Button newButton, refreshButton;
	
	String path = Environment.getExternalStorageDirectory().toString() + "/VICNote";
	String lastOfPath;
	File f = new File(path);

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        setContentView(R.layout.activity_main);

        newButton = (Button) findViewById(R.id.buttonNew);
        newButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent newNote = new Intent(MainActivity.this, NewNoteActivity.class);
				startActivity(newNote);
				finish();
			}
		});
        
        refreshButton = (Button) findViewById(R.id.buttonRefresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				refresh();
			}
		});
        
        File files[] = f.listFiles();
        if(f.listFiles() != null) {
        	final String[] theNamesOfFiles = new String[files.length];
        
	        for(int i = 0; i < theNamesOfFiles.length; i++) {
	        	
	        	lastOfPath = files[i].toString().split("/")[files[i].toString().split("/").length-1];
	        	theNamesOfFiles[i] = lastOfPath.replace(".txt","");
	        	Log.d("Files", "String: " + theNamesOfFiles[i]);
	        }
	
	        ListView lv = (ListView) findViewById(R.id.notesList);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, 
	        															theNamesOfFiles);
	        lv.setAdapter(adapter);
	        lv.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					Log.d("LIST", "File: " + theNamesOfFiles[position]);
					
					Intent editNote = new Intent(MainActivity.this, NewNoteActivity.class);
					editNote.putExtra("noteName", theNamesOfFiles[position] + ".txt");
					startActivity(editNote);
					finish();
				}
			});
        }

        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
        	refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void refresh() {
    	Intent intent = getIntent();
		finish();
		startActivity(intent);
    }
    
}