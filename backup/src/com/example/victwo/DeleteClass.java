package com.example.victwo;

import java.io.File;
import android.os.Environment;
 	

public class DeleteClass {
	String textData, textTitle;
	
	public DeleteClass(String text, String title){
		super();
		this.textData = text;
		this.textTitle = title;
		
		DeleteNote(textData, textTitle);
	}
	
	private void DeleteNote(String text, String title) {
		File path = new File(Environment.getExternalStorageDirectory(), "VICNote" );
		
		File forDelete = new File(path, title + ".txt");
		
		boolean deleted = forDelete.delete();
	}
	
}
