package com.example.victwo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewNoteActivity extends ActionBarActivity {
	
	Button saveButton;
	EditText textData, textTitle;

	String text, title;
	String path = Environment.getExternalStorageDirectory().toString() + "/VICNote";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_note);
		
		textTitle = (EditText) findViewById(R.id.editTitle);
		
		if(getIntent() != null && getIntent().getExtras() != null) {
			Bundle b = getIntent().getExtras();
			title = b.getString("noteName");
			textTitle.setText(title.replace(".txt",""));

			String collected = null;
            FileInputStream fis = null;
            Log.d("FilePATH", path + "/" + title);
            
            try {
            	Log.d("FilePATH", path + "/" + title);

            	fis = new FileInputStream (new File(path + "/" + title));
                byte[] dataArray = new byte[fis.available()];

                while(fis.read(dataArray) != -1){
                	collected = new String(dataArray);
                    Log.d("LOOP", collected);
                }
            } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }
            
            textData = (EditText) findViewById(R.id.editNote);
            textData.setText(collected);
			
			Log.d("EDIT", "File: " + title);
		}
		
		textTitle.setHint("Enter title..");
		
		textData = (EditText) findViewById(R.id.editNote);
		textData.setHint("Enter your note..");
		
		saveButton = (Button) this.findViewById(R.id.buttonSave);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				text = textData.getText().toString();
				title = textTitle.getText().toString();
				
				Log.v("ADebugTag", "string: \"" + text + "\" end of note text");
				new SaveClass(text, title);
				Toast toast = Toast.makeText(getApplicationContext(), "Note saved", Toast.LENGTH_SHORT);
				toast.show();
				endActivity();
			}
		});
		
		Button deleteButton = (Button) this.findViewById(R.id.buttonDelete);
		deleteButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				text = textData.getText().toString();
				title = textTitle.getText().toString();
				
				Log.v("ADebugTag", "string: \"" + text + "\" end of note text");
				new DeleteClass(text, title);
				Toast toast = Toast.makeText(getApplicationContext(), "Note deleted.", Toast.LENGTH_SHORT);
				toast.show();
				endActivity();
			}
		});	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_note, menu);
		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			endActivity();
			Log.d("BACK", "Back button pressed");
		}
		return super.onKeyUp(keyCode, event);
	}

	private void endActivity() {
		Intent newNote = new Intent(NewNoteActivity.this, MainActivity.class);
		startActivity(newNote);
		finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}