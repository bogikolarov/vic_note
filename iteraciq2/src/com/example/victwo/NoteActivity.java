package com.example.victwo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.elsys.vicnote.R;

public class NoteActivity extends ActionBarActivity implements OnClickListener {
	
	boolean play = false;
	boolean record = true;
	
	EditText textData, textTitle;

	String text, title = "";
	String path = Environment.getExternalStorageDirectory().toString() + "/VICNote";
	
	Audio audio = new Audio();
	PopupAudio popup = new PopupAudio(audio);
	
	private static int TAKE_PICTURE = 1;    
	File photo;
	Button overBitmapButton;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);
		overBitmapButton = (Button)findViewById(R.id.buttonOverBitmap);
		textTitle = (EditText) findViewById(R.id.editTitle);
		
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			android.app.ActionBar bar = getActionBar();
			bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		}
		
		
		if(isExistingNote()) {
			Bundle b = getIntent().getExtras();
			Note note = new Note(b);
			
			title = note.getTitle();
			text = note.getTextData();
			textTitle.setText(title.replace(".txt",""));

            if(!text.equals("")) {
	            textData = (EditText) findViewById(R.id.editNote);
	            //Log.d("COLLECTED", "text = " + collected);
	            textData.setText(text);
            }
            
			Log.d("EDIT", "File: " + title);
			
			
			String folder = title.replace(".txt","");
			if(isExistingPhoto(folder)){
				photo = new File(Environment.getExternalStorageDirectory(), "VICNote/" + folder + "/Pic.jpg");
				setBitmapImage();	            
				overBitmapButton.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                // TODO Auto-generated method stub
		            	Intent intent = new Intent();
		            	intent.setAction(Intent.ACTION_VIEW);
		            	intent.setDataAndType(Uri.parse(photo.toString()), "image/*");
		            	startActivity(intent);
		            }
		        });
			}else{
				
			}
		}
		
		//---------------------------------------------------------------------

		textTitle.setHint("Enter title..");
		
		textData = (EditText) findViewById(R.id.editNote);
		textData.setHint("Enter your note..");
	}
	
	@SuppressLint("SimpleDateFormat")
	private String getDateAndTime() {
		String dateTime = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date());
		Log.d("GET DATE", dateTime);
		return dateTime;
	}
	
	private boolean isExistingNote() {
		if(getIntent() != null && getIntent().getExtras() != null)
			return true;
		else
			return false;
	}
	
	private boolean isExistingPhoto(String folder){
		File photo = new File(Environment.getExternalStorageDirectory(), "VICNote/" + folder + "/Pic.jpg");
		if(photo.exists())
			return true;
		return false;
	}
	
	private void toastShow(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	private void endActivity() {
			Intent newNote = new Intent(NoteActivity.this, MainActivity.class);
			startActivity(newNote);
			finish();
		}
	
	private void saveNote() {
		text = textData.getText().toString();
		
		setTitle();
		
		new FileClass(text, title);
		toastShow("Saved.");	
	}
	
	private void setTitle() {
		title = textTitle.getText().toString();
		
		if(title.equals("")) {
			title = getDateAndTime();
		}

	}
	
	@Override	
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.note, menu);
		return true;
	}


	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			File photo = new File(Environment.getExternalStorageDirectory(),  "/VICNote/Pic.jpg");
			if(photo.exists())
				photo.delete();
			endActivity();
			Log.d("BACK", "Back button pressed");
		}
		return super.onKeyUp(keyCode, event);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		
		switch(id) {				
			case R.id.action_camera:
				takePhoto();
				return true;
				
			case R.id.action_audio:
				
				if(popup.popupAudio) {
					setTitle();
					String audioPath = path + "/" + title.replace(".txt", "") + "/" + title.replace(".txt", "") + ".3gpp";
					
					Log.d("AUDIO_PATH", audioPath);
					audio.setPath(audioPath);
					
					popup.init(getApplicationContext());
					popup.popupInit();
					popup.showPopUp(this.findViewById(R.id.action_audio));
					
					popup.popupAudio = false;
				}else {
					popup.popupDismiss();
					popup.popupAudio = true;
				}
				return true;
				
			case R.id.action_delete:
				new DeleteClass(text, title);
				toastShow("Note deleted.");
				endActivity();
				return true;

			case R.id.action_save: 
				saveNote();
				endActivity();
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	public void takePhoto() {
	    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
	    photo = new File(Environment.getExternalStorageDirectory(),  "/VICNote/Pic.jpg");
	    intent.putExtra(MediaStore.EXTRA_OUTPUT,
	            Uri.fromFile(photo));
	    startActivityForResult(intent, TAKE_PICTURE);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    if(TAKE_PICTURE == 1)
	        if (resultCode == Activity.RESULT_OK) {
	        	setBitmapImage();
	        }
	}
	
	public void setBitmapImage(){
		Uri selectedImage = Uri.fromFile(photo);
        getContentResolver().notifyChange(selectedImage, null);
        ImageView imageView = (ImageView) findViewById(R.id.ivReturnedPic);
        ContentResolver cr = getContentResolver();
        Bitmap bitmap;
        try {
             bitmap = android.provider.MediaStore.Images.Media
             .getBitmap(cr, selectedImage);

            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                    .show();
            Log.e("Camera", e.toString());
        }
	}
}